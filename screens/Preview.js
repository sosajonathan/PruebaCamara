import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

import { withNavigation, StackActions, NavigationActions } from 'react-navigation';

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Preview' })],
});





class Preview extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Pagina preview</Text>
        {this.props.navigation.dispatch(resetAction)}
      </View>
    );
  }
}

export default withNavigation(Preview);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerMessage: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  preview: {
    flex: 2
  },
  captureContainer: {
    position: 'absolute',
    flexDirection: 'row',
    right: 10,
    bottom: 10,
    alignItems: 'center',
    zIndex: 1
  },
  captureChildContainer: {
    backgroundColor: 'white',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  message: {
    flex: 1,
    textAlign: 'center',
  }
});